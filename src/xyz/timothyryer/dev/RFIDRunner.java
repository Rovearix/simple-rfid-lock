package xyz.timothyryer.dev;

import java.sql.*;
import java.util.Date;
import java.util.Scanner;

public class RFIDRunner {

    //Database config info
    private static String dbLocation = "jdbc:mysql://localhost:3306/door_lock";
    private static String dbUser = "user";
    private static String dbPass = "pass";

    //Object creation
    private static Door door = new Door();

    //Stores timeout data
    private static boolean timeout = false;

    //Program startup
    public static void main(String[] args) throws Exception {
        //Start listening for any rfid signal
        rfidListen();
    }

    //Listens for a RFID Signal
    public static void rfidListen() {

        //Console Log
        System.out.println("Listening for RFID Signal");

        //RFID Scanner
        Scanner reader = new Scanner(System.in);
        String rfid = reader.next();

        //System to ignore accidental card read
        if (!timeout)
            if (!isEditing())
                rfidLookUp(rfid);
            else {
                System.out.println("Editing mode Enabled!");
                addUser(rfid);
                rfidListen();
            }
        else {
            System.out.println("Cooling down!");
            rfidListen();
        }
    }

    //Checks if in editing mode
    public static boolean isEditing() {
        boolean editing = false;
        try {
            Connection dbConn = DriverManager.getConnection(dbLocation, dbUser, dbPass);
            Statement statement = dbConn.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT editing FROM administration");
            while (resultSet.next())
                editing = resultSet.getBoolean(1);
            statement.close();
            resultSet.close();
            dbConn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return editing;
    }

    //Finds user from rfid
    public static void rfidLookUp(String rfid) {
        try {
            Connection dbConn = DriverManager.getConnection(dbLocation, dbUser, dbPass);
            Statement statement = dbConn.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT name FROM access_list WHERE rfid_code = \"" + rfid + "\" AND access >= " + (1 + getSummer()));
            while (resultSet.next()) {
                logSuccess(resultSet.getString(1));
                door.openDoor();
                cool();
            }
            statement.close();
            resultSet.close();
            dbConn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        //Re-enable listener
        rfidListen();
    }

    //Makes a new thread to stop double user input
    public static void cool() {
        Thread coolant = new Thread(() -> {
            timeout = true;
            try {
                Thread.sleep(1000);
            } catch (Exception e) {
                e.printStackTrace();
            }
            timeout = false;
        });
        coolant.start();
    }

    //Logs when someone gets into the room
    public static void logSuccess(String name) {
        try {
            Connection dbConn = DriverManager.getConnection(dbLocation, dbUser, dbPass);
            PreparedStatement statement = dbConn
                    .prepareStatement("INSERT INTO access_log (name, datetime) VALUES (\"" + name + "\", \"" + new java.sql.Timestamp((new Date()).getTime()) + "\")");
            statement.execute();
            statement.close();
            dbConn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Adds new user to the database
    public static void addUser(String rfid) {
        try {
            Connection dbConn = DriverManager.getConnection(dbLocation, dbUser, dbPass);
            PreparedStatement statement = dbConn
                    .prepareStatement("INSERT INTO access_list (user_id, access, name, rfid_code) VALUES (" + getNextID() + ", 1, \"New Member\", \"" + rfid + "\")");
            statement.execute();
            statement.close();
            dbConn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Finds next available user id
    public static int getNextID() {
        int id = 1;
        try {
            Connection dbConn = DriverManager.getConnection(dbLocation, dbUser, dbPass);
            Statement statement = dbConn.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT user_id FROM access_list");
            while (resultSet.next()) {
                //Get's next biggest number
                int result = resultSet.getInt(1);
                if (id < result)
                    id = resultSet.getInt(1);
            }
            statement.close();
            resultSet.close();
            dbConn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        //Returns one more then the last created user id
        return id + 1;
    }

    //Check for restricted access in the summer
    public static int getSummer() {
        int summer = 0;
        try {
            Connection dbConn = DriverManager.getConnection(dbLocation, dbUser, dbPass);
            Statement statement = dbConn.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT summer FROM administration");
            while (resultSet.next())
                summer = resultSet.getInt(1);
            statement.close();
            resultSet.close();
            dbConn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        //Console Logging
        if (summer == 1)
            System.out.println("It's summer time!");

        //Returns the database value for summer
        return summer;
    }
}