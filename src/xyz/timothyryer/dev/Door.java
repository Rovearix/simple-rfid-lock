package xyz.timothyryer.dev;

import com.pi4j.io.gpio.*;

public class Door {

    //GPIO Variables
    private GpioController gpio;
    private GpioPinDigitalOutput pin;
    private int miliseconds = 3000;

    public Door() {
        //Console Logging
        System.out.println("<--Pi4J--> GPIO Control Pin 12 started.");

        //Create the GPIO Controller
        gpio = GpioFactory.getInstance();

        //Provision GPIO pin #01 as an output pin and turn on
        pin = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_01, "MyRFID", PinState.HIGH);

        //Sets shutdown state for this pin
        pin.setShutdownOptions(true, PinState.LOW, PinPullResistance.OFF);
    }

    //Closes the circuit to send 12v+ to the door strike
    public void openDoor() throws Exception {

        //Lower the voltage to trigger door opening
        System.out.println("Voltage Low");
        System.out.println("--> Door Open");
        pin.low();
        Thread.sleep(miliseconds);

        //Raise the voltage to re-lock the door
        System.out.println("Voltage High");
        System.out.println("--> Door Closed");
        pin.high();
    }
}
